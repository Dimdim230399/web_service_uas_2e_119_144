/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.4.11-MariaDB : Database - myphone
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`myphone` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `myphone`;

/*Table structure for table `informasi` */

DROP TABLE IF EXISTS `informasi`;

CREATE TABLE `informasi` (
  `id_info` int(11) NOT NULL AUTO_INCREMENT,
  `id_merk` int(11) NOT NULL,
  `id_type` int(11) NOT NULL,
  `ram` varchar(25) NOT NULL,
  `cpu` varchar(25) NOT NULL,
  `kamera` varchar(25) NOT NULL,
  `os` varchar(25) NOT NULL,
  `detail` text NOT NULL,
  `photos` varchar(25) NOT NULL,
  `harga` int(11) NOT NULL,
  PRIMARY KEY (`id_info`),
  KEY `id_merk` (`id_merk`),
  KEY `id_type` (`id_type`),
  CONSTRAINT `informasi_ibfk_1` FOREIGN KEY (`id_merk`) REFERENCES `merk` (`id_merk`),
  CONSTRAINT `informasi_ibfk_2` FOREIGN KEY (`id_type`) REFERENCES `typehp` (`id_type`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4;

/*Data for the table `informasi` */

insert  into `informasi`(`id_info`,`id_merk`,`id_type`,`ram`,`cpu`,`kamera`,`os`,`detail`,`photos`,`harga`) values 
(1,7,1,'18gb/23gb','Qualcomm','23mp','qulahjd','best performance with big ram','DC20200515170440.jpg',200000),
(2,2,2,'4gb/16gb','snapdragon 5543',' 8mp/13mp','Android mashmellow','Samsung J7 gadged keluaran terbari dari samsung ','Samsung_j7.jpg',200000),
(3,1,16,'4gb/64gb','Exynos 9610','25mp/12mp','Android 9.0','Samsung A50','samsungA50.jpg',2699000),
(4,1,17,'12mp/8mp','Exynos 8895','12mp/8mp','Android 7.0','Samsung s8','samsungs8.jpg',4499000),
(5,1,18,'8gb/128gb','Exynos 9810','8gb/128gb','Android 9.0','Samsung S10','samsungs10.jpg',6099000),
(6,2,13,'4gb/64gb','Snapdragon 665 ','48mp/13mp','Android Pie','Xiaomi note 9','xiaominote8.jpg',2500000),
(7,2,14,'4gb/64gb','MediaTek Helio G85','48mp/15mp','Android 10','Xiaomi note 9','xiaominote9.jpg',3500000),
(8,2,15,'6gb/128gb','Snapdragon865','108mp/20mp','Android 10','Xiaomi Mi 10 Pro 5G','xiaomi10pro.jpg',9000000),
(10,6,21,'23gb/12gb','Apple A13 Bionic','12mp/12mp','iOS 11','iPhone 11 pro max','iphonepromax.jpg',15000000),
(21,7,22,'18gb/23gb','Qualcomm','23mp','MottoRolla Zx','best performance with big ram','DC20200515151607.jpg',3000000),
(23,11,29,'1gb/8gb','nano','8mp/5mp','Mediatek','best performance','DC20200518152842.jpg',700000),
(24,7,22,'hdhhd','hdhhd','hdhhd','hxhjx','vxbhd','DC20200516100325.jpg',2343),
(31,7,30,'4gb/64gb','snapdragon 675','18mp/12mp','android 10','hape baru','DC20200518150656.jpg',3000000),
(32,13,43,'13gb/4gb','snapdragon','15mp/8mp','android 8','hp murahh','DC20200518152620.jpg',2000000);

/*Table structure for table `merk` */

DROP TABLE IF EXISTS `merk`;

CREATE TABLE `merk` (
  `id_merk` int(11) NOT NULL AUTO_INCREMENT,
  `nama_merk` varchar(25) NOT NULL,
  PRIMARY KEY (`id_merk`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

/*Data for the table `merk` */

insert  into `merk`(`id_merk`,`nama_merk`) values 
(1,'Samsung'),
(2,'Xiaomi'),
(3,'Realmi'),
(4,'Sonny'),
(5,'Nokia'),
(6,'Apple'),
(7,'Motorola'),
(11,'Ht'),
(12,'Vivo'),
(13,'asus');

/*Table structure for table `typehp` */

DROP TABLE IF EXISTS `typehp`;

CREATE TABLE `typehp` (
  `id_type` int(11) NOT NULL AUTO_INCREMENT,
  `nama_type` varchar(25) NOT NULL,
  `id_merk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_type`),
  KEY `id_merk` (`id_merk`),
  CONSTRAINT `typehp_ibfk_1` FOREIGN KEY (`id_merk`) REFERENCES `merk` (`id_merk`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4;

/*Data for the table `typehp` */

insert  into `typehp`(`id_type`,`nama_type`,`id_merk`) values 
(1,'Samsung J7',1),
(2,'Xiaomi readmi 3',2),
(4,'Realmi note 2',3),
(5,'Sonny Experia Z',4),
(6,'Nokia N73',5),
(7,'iPhone x',6),
(8,'MotoRolla X',7),
(12,'Xiaomi note 3',2),
(13,'Xiaomi note 8',2),
(14,'Xiaomi note 9',2),
(15,'Xiaomi Mi 10 Pro 5G',2),
(16,'Samsung A50',1),
(17,'Samsung S8',1),
(18,'Samsung S10',1),
(19,'iPhone 8 Plus',6),
(20,'iPhone XS',6),
(21,'iPhone 11 Pro Max',6),
(22,'MotoRolla Z',7),
(23,'Lumia A51',5),
(24,'Realmi note 4',3),
(25,'Sony Experia Z aqua',4),
(27,'data_dummy',11),
(28,'data_dummy',12),
(29,'Ht ipQ',11),
(30,'Motorolla A',7),
(31,'Motorolla B',7),
(32,'Motorolla C',7),
(33,'Nokia A',5),
(34,'Nokia B',5),
(35,'Nokia C',5),
(36,'Realme A',3),
(37,'Realme B',3),
(38,'Realme C',3),
(39,'Sony A',4),
(40,'Sony B',4),
(41,'Sony C',4),
(42,'data_dummy',13),
(43,'asus zenfone 4',13);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
